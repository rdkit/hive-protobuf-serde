package com.ivyft.hive.serde2;

import com.ivyft.hive.serde2.entity.Entities;
import com.ivyft.hive.serde2.protobuf.IntLengthHeaderFile;
import com.ivyft.hive.hadoop.IntLengthHeaderInputFormat;
import com.ivyft.hive.serde2.protobuf.ProtobufSerde;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.serde.Constants;
import org.apache.hadoop.io.BytesWritable;
import org.junit.Test;

import java.net.URI;
import java.util.Arrays;
import java.util.Properties;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/11/8
 * Time: 15:32
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class HeaderRecordReader1Test {




    protected static Log LOG = LogFactory.getLog(HeaderRecordReader1Test.class);



    public HeaderRecordReader1Test() {

    }


    @Test
    public void testRead() throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(new URI("file:///"), conf);
        IntLengthHeaderFile.Reader reader = new IntLengthHeaderFile.Reader(fs, new Path("./data/location20.dat"));

        ProtobufSerde protobufSerde = new ProtobufSerde();
        Properties prop = new Properties();
        prop.setProperty(IntLengthHeaderInputFormat.PROTOBUF_CLASS, "com.ivyft.hive.serde2.entity.Entities$Location");
        prop.setProperty(Constants.LIST_COLUMNS, "location,  loccity,   citylevel,   locProvince");


        protobufSerde.initialize(conf, prop);

        int i = 0;
        while (reader.hasNext()){
            i++;
            BytesWritable next = reader.next();
            Object deserialize = protobufSerde.deserialize(next);

            System.out.println(i + "    " + next.getLength() + "    " + deserialize);
            System.out.println(Arrays.toString(next.getBytes()));

            if(i == 2) {
                break;
            }
        }
        reader.close();
    }


    @Test
    public void testParseError() throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(new URI("file:///"), conf);
        IntLengthHeaderFile.Reader in = new IntLengthHeaderFile.Reader(fs, new Path("./data/location20.dat"));

        BytesWritable content = null;
        if(in.hasNext()) {
            content = in.next();
        }

        BytesWritable value = new BytesWritable();

        if(content != null) {
            value.set(content);
            value.setCapacity(content.getLength());

            //value.set(content.getBytes(), 0, content.getLength());
        }

        System.out.println(Entities.Location.parseFrom(value.getBytes()));

        System.out.println("======================next=======================" + value.getLength());
        System.out.println("next" + Arrays.toString(value.getBytes()));

    }
}
