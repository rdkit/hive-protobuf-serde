package com.ivyft.hive.serde2;

import com.ivyft.hive.serde2.entity.Entities;
import com.ivyft.hive.serde2.protobuf.IntLengthHeaderFile;
import com.ivyft.hive.hadoop.IntLengthHeaderInputFormat;
import com.ivyft.hive.serde2.protobuf.ProtobufSerde;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.serde.Constants;
import org.apache.hadoop.io.BytesWritable;
import org.junit.Test;

import java.net.URI;
import java.util.Properties;
import java.util.Random;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/11/7
 * Time: 22:58
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class ProtobufSerdeTest {



    private String[] locs = new String[]{"中国", "美国", "德国", "日本", "法国", "英国", "俄罗斯"};


    public ProtobufSerdeTest() {
    }



    @Test
    public void testSerFile() throws Exception {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(new URI("file:///"), conf);
        IntLengthHeaderFile.Writer writer = new IntLengthHeaderFile.Writer(fs, new Path("./data/location500w.dat"));

        Random r = new Random();
        for (int i = 0; i <500*10000; i++) {
            Entities.Location.Builder builder = Entities.Location.newBuilder();
            builder.setLocation(locs[r.nextInt(locs.length)]).setLocCountry(r.nextInt(200)).setCityLevel(r.nextInt(5));
            Entities.Location location = builder.setLocProvince(r.nextInt(34)).setLocCity(r.nextInt(400)).build();

            writer.write(location.toByteArray());

            if(i % 1000 == 0) {
                writer.flush();
                System.out.println("=============="+i+"==============");
            }
        }

        writer.flush();
        writer.close();
    }



    @Test
    public void testSerde() throws Exception {
        ProtobufSerde protobufSerde = new ProtobufSerde();
        Properties prop = new Properties();
        prop.setProperty(IntLengthHeaderInputFormat.PROTOBUF_CLASS, "com.ivyft.hive.serde2.entity.Entities$Location");
        prop.setProperty(Constants.LIST_COLUMNS, "location,  loccity,   citylevel,   locProvince");



        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(new URI("file:///"), conf);
        IntLengthHeaderFile.Reader reader = new IntLengthHeaderFile.Reader(fs, new Path("./data/location50.dat"));
        reader.hasNext();
        BytesWritable next = reader.next();

        reader.close();

        protobufSerde.initialize(conf, prop);


        Object deserialize = protobufSerde.deserialize(next);

        System.out.println(deserialize);


    }



    @Test
    public void testSerdeEnum() throws Exception {
        Entities.OSBrowser.Builder builder = Entities.OSBrowser.newBuilder();
        builder.setName("中国").setType(Entities.OSBrowser.Type.BROWSER);
        Entities.OSBrowser browser = builder.setUrn(2).build();
        System.out.println(browser);


        ProtobufSerde protobufSerde = new ProtobufSerde();
        Properties prop = new Properties();
        prop.setProperty(IntLengthHeaderInputFormat.PROTOBUF_CLASS, Entities.OSBrowser.class.getName());
        prop.setProperty(Constants.LIST_COLUMNS, "name,  type,   urn");

        Configuration conf = new Configuration();

        protobufSerde.initialize(conf, prop);

        Object deserialize = protobufSerde.deserialize(new BytesWritable(browser.toByteArray()));

        System.out.println(deserialize);
    }




    @Test
    public void testSerdeEnumPeople() throws Exception {
        Entities.People.Builder builder = Entities.People.newBuilder();
        builder.setGender(Entities.Gender.U).addFriends("zhenqin").addFriends("java");


        Entities.OSBrowser.Builder builderx = Entities.OSBrowser.newBuilder();
        builderx.setName("中国").setType(Entities.OSBrowser.Type.BROWSER);
        Entities.OSBrowser browser = builderx.setUrn(2).build();

        Entities.People people = builder.setOsBrowser(browser).build();
        System.out.println(people);

        ProtobufSerde protobufSerde = new ProtobufSerde();
        Properties prop = new Properties();
        prop.setProperty(IntLengthHeaderInputFormat.PROTOBUF_CLASS, Entities.People.class.getName());
        prop.setProperty(Constants.LIST_COLUMNS, "name,  gender, old, num, friends, osBrowser");
        prop.setProperty(Constants.LIST_COLUMN_TYPES, "string:string:int:int:array<string>:map<string,string>");

        Configuration conf = new Configuration();

        protobufSerde.initialize(conf, prop);

        Object deserialize = protobufSerde.deserialize(new BytesWritable(people.toByteArray()));

        System.out.println(deserialize);


//        ObjectInspector objectInspector = protobufSerde.getObjectInspector();
//        System.out.println(objectInspector.getTypeName());
//        System.out.println(objectInspector.getCategory());

    }
}
